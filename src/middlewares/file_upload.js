const multer = require("multer");

const profile_image = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "images")

        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + "_" + Date.now() + ".png")

        }
    })
}).single("image");

module.exports = profile_image;