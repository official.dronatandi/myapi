const express = require("express");
const bodyParser = require('body-parser');
const profile_router = express();

profile_router.use(bodyParser.json());
profile_router.use(bodyParser.urlencoded({extended: true}));

const multer = require("multer");
const path = require("path");

profile_router.use(express.static('public'));

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../../public/profileImages'), function (error, success) {
            if (error) {
                throw error;
            }
        });
        
    },

    filename: function (req, file, cb) {
        const file_name = Date.now() + '-' + file.originalname;

        cb(null, file_name, function (error1, success1) {
            if(error1) throw error1
        })
    }

});


const upload = multer({ storage: storage });


const profile_controller = require("../controllers/profileController");

profile_router.post("/profile", upload.single('image'), profile_controller.createProfile);

module.exports = profile_router;
