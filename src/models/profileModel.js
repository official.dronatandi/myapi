const mongoose = require("mongoose");

const ProfileSchema = mongoose.Schema({
    username: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true

    },
    image:
    {
        type: String,
    },
    mobile: {
        type: String,
        require: true
    },
    type: {
        type: Number,
        require: true
    }
}, { timestamps: true });

module.exports = mongoose.model("Profile", ProfileSchema);